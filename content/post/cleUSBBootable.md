---
title: Comment créer une clé usb bootabla
date: 2020-01-01
---

# Téléchargement

Téléchager la bonne image iso


# Linux Mint

Aller dans les applications et choisir "Créateur de clé usb". Chercher l'iso et mettre la clé à formater.


# Windows

Essayer avec ["ISO to USB"](https://iso-to-usb.fr.softonic.com/)