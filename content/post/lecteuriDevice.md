---
title: Rhytmbox le lecteur iDevice de linux
date: 2019-09-01
tags: ["linux"]
---


Pour ajouter l'application Rhytmbox pour lire les iDevice (iPad, iPod, iPhone) sur linux

```
sudo add-apt-repository ppa:fossfreedom/rhythmbox-plugins
sudo apt-get update
sudo apt-get install rhythmbox-plugin-complete
```