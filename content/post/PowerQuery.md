---
title: Le Power Query
subtitle: dans Excel, via l'éditeur de script
date: 2020-04-17
tags: ["excel"]
---


# Remarques

Remarque : 
* nom colonne sensible à la case
* mettre une ',' à la fin de chaque instruction sauf la dernière ligne
* On reprend à chaque ligne le tableau précédent ; sauf si on veut créer plusieurs tableaux


# La source à partir d'un tableau

* Créer d'abord un tableau dans la feuille (Insertion -> Tableau) et le nommer "MonTab"
```m
let
    Source = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content]
in
    Source
```

* Modification des types
```m
let
    Source = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content],
    ModifType = Table.TransformColumnTypes(Source,{{"Titre", type text},{"Nombre", type number},{"Date Naissance", type date}, {"Affectation", Int64.Type}}),
    TabOut = ModifType
in
    TabOut
```

* Sélectionner des lignes à plusieurs critères

```
let
    Source = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content],
    AffecDistinct = Table.SelectRows(Source, each ([KL_NbMatricule.NbMatricule] = 1) or ([#"Fin_affectation_$$$$$"] = null and [KL_NbMatricule.NbMatricule] >= 1)),
    TabOut = AffecDistinct
in
    TabOut
```


# Formule simple

* Ajouter une colonne avec du texte en dur
```sql
let
    Source = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content],
    AjoutCol = Table.AddColumn(AjoutCol , "MaColonneT", each "Valeur ajoutée"),
    AjoutColCell = Table.AddColumn(AjoutCol , "MaColonneT", each Source[mail]),
	
    TabOut = AjoutCol
in
    TabOut

```

* Ajouter une colonne avec un `SI

```sql
let
    Source = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content],
	AjoutCol = Table.AddColumn(Source, "MaColonne", each if [NomDeMaColonne] = "1" then "C'est le 1" else "C'est l'autre"),
	TabOut = AjoutCol
in
    TabOut
```


# Jointure

```sql
let
    MonTab = Excel.CurrentWorkbook(){[Name="MonTab"]}[Content], // si requête non créée
    TabXML = Excel.CurrentWorkbook(){[Name="TabXML"]}[Content], // si requête non créée
    TabJoint = Table.NestedJoin(MonTab ,{"email"},TabXML,{"mail"},"NewColonne",JoinKind.Inner)
in
    TabJoint
```



# Compter

```sql
let
	Source = Excel.CurrentWorkbook(){[Name="Tableau1"]}[Content],
	CountNb = Table.Group(Source , {"nombre"}, {{"Nombre.1", each Table.RowCount(_), type number}}),
in
    TabJoint
```


# Expand all
Sur les jointures, pour ne pas réécrire toutes les colonne dans la fonction expand, on peut écrire une fonction personnalisée


```sql
// From Chris Webb's blog - http://blog.crossjoin.co.uk/2014/05/21/expanding-all-columns-in-a-table-in-power-query/

let
    //Define function taking two parameters - a table and an optional column number 
    Source = (TableToExpand as table, optional ColumnNumber as number) =>
    let
     //If the column number is missing, make it 0
     ActualColumnNumber = if (ColumnNumber=null) then 0 else ColumnNumber,
     //Find the column name relating to the column number
     ColumnName = Table.ColumnNames(TableToExpand){ActualColumnNumber},
     //Get a list containing all of the values in the column
     ColumnContents = Table.Column(TableToExpand, ColumnName),
     //Iterate over each value in the column and then
     //If the value is of type table get a list of all of the columns in the table
     //Then get a distinct list of all of these column names
     ColumnsToExpand = List.Distinct(List.Combine(List.Transform(ColumnContents, 
                        each if _ is table then Table.ColumnNames(_) else {}))),
     //Append the original column name to the front of each of these column names
     NewColumnNames = List.Transform(ColumnsToExpand, each ColumnName & "." & _),
     //Is there anything to expand in this column?
     CanExpandCurrentColumn = List.Count(ColumnsToExpand)>0,
     //If this column can be expanded, then expand it
     ExpandedTable = if CanExpandCurrentColumn 
                         then 
                         Table.ExpandTableColumn(TableToExpand, ColumnName, 
                                ColumnsToExpand, NewColumnNames) 
                         else 
                         TableToExpand,
     //If the column has been expanded then keep the column number the same, otherwise add one to it
     NextColumnNumber = if CanExpandCurrentColumn then ActualColumnNumber else ActualColumnNumber+1,
     //If the column number is now greater than the number of columns in the table
     //Then return the table as it is
     //Else call the ExpandAll function recursively with the expanded table
     OutputTable = if NextColumnNumber>(Table.ColumnCount(ExpandedTable)-1) 
                        then 
                        ExpandedTable 
                        else 
                        ExpandAll(ExpandedTable, NextColumnNumber)
    in
     OutputTable
in
    Source
```