---
title: NotePad ++
subtitle: ... l'indispensable
date: 2020-04-11
tags: ["expression régulière","npp"]
---


# Rechercher / Remplacer

## Mode étendue


* `^` : l'accent circonflexe désigne le début d'une ligne.
* `$` : le dollar désigne la fin d'une ligne.
* `^.`: désigne “n'importe quel caractère en début de ligne”.


Source : [nliautaud](http://nliautaud.fr/wiki/articles/notepadpp/expreg)

|Rechercher|Remplacer|Signification|
|:------|:------|:------|
|`^`|A|Ajoute en début de ligne la lettre A|
|`$`|Z|Ajoute en fin de ligne la lettre Z|
|\r\n| |Enlève sauts de lignes => txt 1 ligne|
|`^.`|A|Remplace n'importe quel premier caractère par la lettre A|



## Expressions régulières

|Rechercher|Remplacer|Signification|
|:------|:------|:------|
|`[0-9]`|toto|Remplace un chiffre par toto|
|(`[0-9]`)|nbr\1|Remplace un chiffre par nbre#le chiffre#. La parenthèse permet de nommer par bloc \1, \2, ...|
|`([0-9][0-9][0-9][0-9][0-9])`|`\(\1\)`|Un code postal|
|`\s`| |Rechercher un espace par un espace|
|`\s+`| |Rechercher un/plusieurs espace(s)|





# Macro
## Enregistrer macro
* Aller dans Macro
* Cliquer sur "Démarrer l'enregistrement"
* Une fois terminé, cliquer sur "Arrêter l'enregistrement"
* Cliquer sur "Enregistrer la macro"

## Modifier macro
### Application installée
* Aller dans le dossier C:\Users\#user#\AppData\Roaming\Notepad++
* Ouvrir le fichier shortcuts.xml

### Application en portable
* Aller dans le dossier _repertoireinstallation_\npp.0.0.0.portable.x64
* Ouvrir le fichier shortcuts.xml

/!\ Quand on enregistre une macro, pour la voir dans le fichier shortcuts.xml, il faut fermer npp.
Il est possible de modifier directement le fichier. De même, fermer npp et le réouvrir pour avoir les modifications prises en compte.

Les macros sont enregistrées entre les balises <Macros></Macros>
Chaque macro est enregistrée entre les balises <Macro></Macro>