---
title: Advent of code - in awk
date: 2022-12-01
tags: ["awk"]
---


# Contexte

Calendrier de l'avent

https://adventofcode.com/2022/

# Mes réponses

## Day 1

### Jeu 1

```awk
BEGIN {i=1}
{if ($1 == "")
    {print i " " S ; S=0 ; i++}
else
    {S+=$1}
}
END {print i " " S}
```
puis

```awk
($2 > n) {n=$2}
END {print n}
```

### Jeu 2

```awk
BEGIN {i=1}
{if ($1 == "")
    {print S ; S=0 ; i++}
else
    {S+=$1}
}
END {print S}
```

```sh
awk -f 01_02_01_list 01_in.txt | sort -n -r | head -3 | awk '{S+=$1} END {print S}'
```


## Day 2

### Jeu 1

```awk
{
# score suivant la réponse
if ($2 == "X") {resp=1 ; S+=resp}
if ($2 == "Y") {resp=2 ; S+=resp}
if ($2 == "Z") {resp=3 ; S+=resp}
# score suivant le gagnant
    ## identique
if ( ($1 == "A" && $2 == "X") || ($1 == "B" && $2 == "Y") || ($1 == "C" && $2 == "Z")) {point=3 ; S+=point}
    ## win
if ( ($1 == "C" && $2 == "X") || ($1 == "A" && $2 == "Y") || ($1 == "B" && $2 == "Z")) {point=6 ; S+=point}
}
END {print S}
```

### Jeu 2

```awk
{
# score suivant le coup
    ## si null donc coup identique
if ($2 == "Y" && $1 == "A") {coup=1 ; S+=coup+3}
if ($2 == "Y" && $1 == "B") {coup=2 ; S+=coup+3}
if ($2 == "Y" && $1 == "C") {coup=3 ; S+=coup+3}
    ## si gagnant
if ($2 == "Z" && $1 == "A") {coup=2 ; S+=coup+6}    # mon coup est un paper
if ($2 == "Z" && $1 == "B") {coup=3 ; S+=coup+6}    # mon coup est un scissor
if ($2 == "Z" && $1 == "C") {coup=1 ; S+=coup+6}    # mon coup est un rock
    ## si perdant
if ($2 == "X" && $1 == "A") {coup=3 ; S+=coup}    # mon coup est un scissor
if ($2 == "X" && $1 == "B") {coup=1 ; S+=coup}    # mon coup est un rock
if ($2 == "X" && $1 == "C") {coup=2 ; S+=coup}    # mon coup est un paper
}
END {print S}
```

## Day 3

### Jeu 1

```awk
BEGIN { low="abcdefghijklmnopqrstuvwxyz" ; upp="ABCDEFGHIJKLMNOPQRSTUVWXYZ"}
{
    part1=substr($1,1,length($1)/2)
    part2=substr($1,length($1)/2 + 1,length($1))
    split(part1, arr, "")
    for (i in arr) {
      if (match(part2, arr[i])) {
            carcom=arr[i]            
            break
            }
    }
    {if (index(low, carcom) == 0 ) {S+= 26 + index(upp, carcom)} else {S+= ind = index(low, carcom)}};
}
END {print S}
```

## Day 4

### Jeu 1

```awk
{
# splitter les caracteres ; initialisation ; reconstruire la chaine de chiffres
split($1, sec1, "-")
split($2, sec2, "-")
# test si la chaine 1 est dans la chaine 2 ou inversement
if ( ( sec1[1] >= sec2[1] && sec1[2] <= sec2[2] ) || sec2[1] >= sec1[1] && sec2[2] <= sec1[2] ) {S+=1}
}
END {print S}
```

Lancer en modifiant le séparateur

> awk -f 04_02_exec FS="," 04_in.txt

### Jeu 2

```awk
{
# splitter les caracteres ; initialisation ; reconstruire la chaine de chiffres
split($1, sec1, "-")
split($2, sec2, "-")
# test si la chaine 1 est dans la chaine 2 ou inversement
if ( ( sec2[1] >= sec1[1] && sec2[1] <= sec1[2] ) || sec1[1] >= sec2[1] && sec1[1] <= sec2[2] ) {S+=1}
}
END {print S}
```


## Day 5

### Jeu 1

Modification de l'attendu pour enlever les "move " "from " "to ". Fait en éditeur de texte (flemme de le faire ici)

```awk
BEGIN {
    rang="ZTFRWJG,GWM,JNHG,JRCNW,WFSBGQVM,SRTDVWC,HBNCDZGV,SJNMGC,GPNWCJDL" ;
    split(rang, R, ",")
    line=9
}
{    
    for (n=1; n<=line; n++) {split(R[n], SYMTAB["arr"n], "")}

    nb=$1
    from=$2
    to=$3

    for (i=1; i<=nb; i++) {
        R[to]=R[to] SYMTAB["arr"from][length(R[from])]
        R[from]=substr(R[from], 1, length(R[from])-1)
    }
}
END {
for (k=1; k<=line; k++) {split(R[k], SYMTAB["arr"k], "")}

printf "Situation finale : "

# afficher le dernier caractère
printf arr1[length(R[1])]
printf arr2[length(R[2])]
printf arr3[length(R[3])]
printf arr4[length(R[4])]
printf arr5[length(R[5])]
printf arr6[length(R[6])]
printf arr7[length(R[7])]
printf arr8[length(R[8])]
printf arr9[length(R[9])]
print ""    
}
```

### Jeu 2

```awk
BEGIN {
    rang="ZTFRWJG,GWM,JNHG,JRCNW,WFSBGQVM,SRTDVWC,HBNCDZGV,SJNMGC,GPNWCJDL" ;
    split(rang, R, ",")
    line=9
}
{    
    for (n=1; n<=line; n++) {split(R[n], SYMTAB["arr"n], "")}

    nb=$1
    from=$2
    to=$3

    for (i=1; i<=nb; i++) {
        R[to]=R[to] SYMTAB["arr"from][length(R[from])-nb+i]
    }
    R[from]=substr(R[from], 1, length(R[from])-nb)
}
END {
for (k=1; k<=line; k++) {split(R[k], SYMTAB["arr"k], "")}

printf "Situation finale : "
# afficher le dernier caractère
printf arr1[length(R[1])]
printf arr2[length(R[2])]
printf arr3[length(R[3])]
printf arr4[length(R[4])]
printf arr5[length(R[5])]
printf arr6[length(R[6])]
printf arr7[length(R[7])]
printf arr8[length(R[8])]
printf arr9[length(R[9])]
print ""    
}
```

## Day 6

### Jeu 1
```awk
BEGIN {nLettreMot=4}
{
    print $0
    # boucle par mot
    for (i=1; i<=length($1); i++) {
        TOT = 0
        # boucle par lettre dans le mot
        for (j=1; j<=nLettreMot; j++) {
            S=0
            # mot : substr($1,i,nLettreMot)
            # lettre : substr(substr($1,i,nLettreMot),j,1)
            for (k=1; k<=nLettreMot; k++) {
                #print substr($1,i,nLettreMot) ": lettre ref " substr(substr($1,i,nLettreMot),j,1) " comparee "  substr(substr($1,i,nLettreMot),k,1)
                if ( substr(substr($1,i,nLettreMot),j,1) == substr(substr($1,i,nLettreMot),k,1)) {
                    S+=1
                }
        if (S >= TOT) {TOT = S}
        #print S " ; " TOT
            }
        }
    if (TOT == 1) {break}
    }
    print "fin : " i + nLettreMot - 1
}
```

### Jeu 2

idem avec nLettreMot=14

# Jour 7

## Jeu 1

```awk
BEGIN {borne=100000}
{
    # commande cd dir
    if  ($1 == "$" && $2 == "cd" && $3 != "..") {
        i++
        dir[i]=$3
    }

    # dans le ls
    if ($2 != "ls" && $3 != ".." && $2 != "cd" ) {
        ## si dir        
        if ($1 == "dir") {
            for (j=1;j<=i;j++) {
                dir[j]=dir[j] "|" $2
            }
        }
        ## si file avec nombre en $1
        if ($1~/^[0-9]+$/) {
            for (j=1;j<=i;j++) {
                somme[j]+=$1
            }
        }
    }

    # si cd .. ; on écrit le réperoire parsé et on ajoute à la somme totale
    if  ($1 == "$" && $2 == "cd" && $3 == "..") {
        if(somme[i] <= borne) {print dir[i] ":" somme[i] ; tot+=somme[i]}
        dir[i]=""
        somme[i]=0
        i--
    }
}
END {for (j=1;j<=length(dir);j++) {
                if(somme[j] <= borne) {print dir[j] ":" somme[j] ; tot+=somme[j]}
            }
   print "Total : " tot
}
```

Optimisation sans les print

```awk
BEGIN {borne=100000}
{
    # commande cd dir
    if  ($1 == "$" && $2 == "cd" && $3 != "..") {
        i++
    }
    # dans le ls
    if ($2 != "ls" && $3 != ".." && $2 != "cd" ) {
        ## si file avec nombre en $1
        if ($1~/^[0-9]+$/) {
            for (j=1;j<=i;j++) {
                somme[j]+=$1
            }
        }
    }
    # si cd .. ; on écrit le réperoire parsé et on ajoute à la somme totale
    if  ($1 == "$" && $2 == "cd" && $3 == "..") {
        if(somme[i] <= borne) {tot+=somme[i]}
        somme[i]=0
        i--
    }
}
END {
# Pour la fin car on n'a pas les cd .. pour revenir à la racine
for (j=1;j<=length(dir);j++) {
                if(somme[j] <= borne) {tot+=somme[j]}
            }
   print "Total : " tot
}
```

### Jeu 2

Avant : connaitre mon total et la taille à libérer
```awk
BEGIN {need=30000000 ; tot=70000000}
{
    if ($1~/^[0-9]+$/) S+=$1
}
END {
    print "Taille occupée : " S
    print "Taille à libérer : " need - (tot - S)
}
```

```awk
BEGIN {L=3837783 ; G = 70000000}
{
    # commande cd dir
    if  ($1 == "$" && $2 == "cd" && $3 != "..") {
        i++
    }

    # dans le ls
    if ($2 != "ls" && $3 != ".." && $2 != "cd" ) {
        ## si file avec nombre en $1
        if ($1~/^[0-9]+$/) {
            for (j=1;j<=i;j++) {
                somme[j]+=$1
            }
        }
    }
    # si cd ..
    if  ($1 == "$" && $2 == "cd" && $3 == "..") {        
        if (somme[i] >= L && somme[i] <= G) {G=somme[i]}        
        somme[i]=0
        i--
    }
}
END {for (j=1;j<=i;j++) {
            if (somme[j] >= L && somme[j] <= G) {G=somme[j]}        
            }
   print "A supprimer : " G
}
```

## Day 8

### Jeu 1

Commande : 
```sh
python 08_01_exec.py < 08_in.txt
```

```py
import sys

#myList=[]
#for s in sys.stdin.read().splitlines():
#    myList.append(s)

myList = list(list(map(int,s)) for s in sys.stdin.read().splitlines())
tot = 0

for n in range(len(myList)):
    for i in range(len(myList[n])): 
        # extremite
        if (n==0 or i==0 or i==len(myList[n])-1 or n==len(myList)-1):
            tot+=1
            continue 
        
        # on regarde a droite
        for j in range(i+1, len(myList[n])):
            if (myList[n][j] >= myList[n][i]):
                d=0
                break
            else:
                d=1
       
         # on regarde a gauche
        for k in range(0, i):
            if (myList[n][k] >= myList[n][i]):
                g=0
                break
            else:
                g=1
        
        # on regarde en haut
        for l in range(0, n):
            if (myList[l][i] >= myList[n][i]):
                h=0
                break
            else:
                h=1
        
        # on regarde en bas
        for m in range(n+1, len(myList)):
            if (myList[m][i] >= myList[n][i]):
                b=0
                break
            else:
                b=1
        
        tot+=(d|g|h|b)

print "Total : " + str(tot)
```

### J2

```awk
import sys

myList = list(list(map(int,s)) for s in sys.stdin.read().splitlines())
S=0
for n in range(len(myList)):
    for i in range(0,len(myList[n])): 
        score=0    
        
        
        # on regarde a droite
        d=0
        for j in range(i+1, len(myList[n])):
            d+=1      
            if (myList[n][i] <= myList[n][j]):
                break
        
        # on regarde a gauche
        g=0
        for k in reversed(range(0, i)):
            g+=1
            if (myList[n][i] <= myList[n][k]):
                break            

        # on regarde en haut
        h=0
        for l in reversed(range(0, n)):
            h+=1        
            if (myList[n][i] <= myList[l][i]):
                break

        # on regarde en bas
        b=0
        for m in range(n+1, len(myList)):
            b+=1        
            if (myList[n][i] <= myList[m][i]):
                break

        S=max(S, d*g*h*b)
        
print S
```

# Day 10

## Jeu 1

```awk
BEGIN {x=1 ; cycle="20,60,100,140,180,220"
        split(cycle, cycle_step, ",")
}
{
    c+=1
    p[c]=x*c
    if ($1 == "addx") {
        c+=1
        p[c]=x*c
        x+=$2
    }
}
END {
    for (i in cycle_step) {
        print i " - " p[cycle_step[i]]
        S+=p[cycle_step[i]]
    }
print "Total : " S
}
```

avec function dans le awk

```awk
BEGIN {
    x=1
    cycle="20,60,100,140,180,220"
}
function inc() {
    c+=1
    p[c]=x*c
}
{
    inc()
    if ($1 == "addx") {
        inc()
        x+=$2
    }
}
END {
    split(cycle, cycle_step, ",")
    for (i in cycle_step) {
        S+=p[cycle_step[i]]
    }
print "Total : " S
}
```

### Jeu 2

Ultra moche en utilisant des tableaux. Voir après pour solution clean.

```awk
BEGIN {x=1+1 ;
CRT ="----------------------------------------"
split(CRT, CRTx1, "")
split(CRT, CRTx2, "")
split(CRT, CRTx3, "")
split(CRT, CRTx4, "")
split(CRT, CRTx5, "")
split(CRT, CRTx6, "")
}
function inc(){
    if (c >= 1) {n=1 ; off=0}
    if (c >= 41) {n=2 ; off=-40}
    if (c >= 81) {n=3; off=-80}
    if (c >= 121) {n=4; off=-120}
    if (c >= 161) {n=5; off=-160}
    if (c >= 201) {n=6; off=-200}        
    if (c+off == x) {SYMTAB["CRTx"n][x]="#"}
    if (c+off == x+1) {SYMTAB["CRTx"n][x+1]="#"}
    if (c+off == x-1) {SYMTAB["CRTx"n][x-1]="#"}
}
{
    c+=1
    inc()
    if ($1 == "addx") {
        inc()    
        c+=1
        inc()
        x+=$2
    }
}
END {
    for (n=1; n<=6; n++) {
        for (p=1; p<=40; p++) {
            printf SYMTAB["CRTx"n][p]
        }
        print ""
    }
}
```
```awk
BEGIN {x=1+1; carOK="#" ; carKO="-"
}
function inc(){
    c+=1
    if (c >= 1) {off=0}
    if (c >= 41) {off=-40}
    if (c >= 81) {off=-80}
    if (c >= 121) {off=-120}
    if (c >= 161) {off=-160}
    if (c >= 201) {off=-200}
    if (index(",40,80,120,160,200,240, ", "," c ",") > 0) {print "" ; return}
    if (c+off == x || c+off == x+1 || c+off == x-1) {printf carOK} else {printf carKO}
}
{if ($1 == "noop") {inc()}}
{
    if ($1 == "addx") {
        inc()    
        inc()
        x+=$2
    }
}
END {
}
```

## Day 11

### Jeu 1

Dans le input, rajouter un saut de ligne à la fin

```py
import sys
import re

item=[]
op=[]
te=[]
iftrue=[]
iffalse=[]
tot=[]

def calculate():
	for m in range(0,len(item)):
		for i in item[m]:
			tot[int(m)]+=1
			if op[m][1] == "old":
				c=int(i)
			else:
				c=int(op[m][1])
			if op[m][0] == "*":
				w = int(i)*c / 3
			if op[m][0] == "+":
				w = (int(i)+int(op[m][1])) / 3
			if op[m][0] == "-":
				w = int(i)-int(op[m][1]) / 3
			if w % int(te[m][0]) == 0:
				mNew=iftrue[m]
			else:
				mNew=iffalse[m]
			# On le met au monkey suivant
			for n in mNew:
				mNew2 = int(n)
			item[mNew2].append(w)
		item[m]=[]
		
for s in sys.stdin.read().splitlines():
	if "Monkey" in s:
		m = int(s.split(' ')[1][0:1])
		item.append("")
		op.append("")
		te.append("")
		iftrue.append("")
		iffalse.append("")
		tot.append(0)
	if "Starting items" in s:
		s = re.sub(',', '', s)
		item[m] = s.split(' ')[4:]
	if "Operation:" in s:
		op[m] = s.split(' ')[6:]
	if "Test:" in s:
		te[m] = s.split(' ')[5:]
	if "If true:" in s:
		iftrue[m] = s.split(' ')[9:]
	if "If false:" in s:
		iffalse[m] = s.split(' ')[9:]


for r in range(0,20):
	calculate()

for m in range(0,len(item)):
	print "Monkey " + str(m) + ": " 
	print item[m]
	print tot[m]
```
