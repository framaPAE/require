---
title: Joplin - éditeur Markdown
date: 2020-11-15
tags: ["Markdown"]
---


# Page

Lien officiel : https://joplinapp.org/


# Installation
## Sous Linux

* Installation avec les livrables du site officiel
* Installation en ligne de commande, cf [Source : tecmint](https://www.tecmint.com/install-joplin-in-linux/)

> wget -O - https://raw.githubusercontent.com/laurent22/joplin/dev/Joplin_install_and_update.sh | bash

# Synchronisation
