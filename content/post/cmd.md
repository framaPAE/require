---
title: La commande en windows
subtitle: ...et oui winbouze
date: 2020-04-10
tags: ["cmd"]
---

# Se balader dans les répertoire

Utiliser dir pour lister les éléments du répertoire courant
Pour enregistrer dans un fichier texte, mettre :
```shell
cd Documents
dir > liste_fichiers.txt
echo Liste des fichiers creee
```

* `cd` : changer de répertoire
* `dir` : lister les éléments
* `echo` : à marquer dans le terminal.


# Créer un fichier

On peut créer un *.bat et en double cliquant dessus ça lance la commande.


```shell
@echo off

dir > liste_fichiers.txt
echo Liste des fichiers creee
PAUSE
```

* `PAUSE` : faire une pause dans le script


# Renommer

On utilise la commande 'rename'

Voici un exemple de *.bat qui renomme à partir d'un fichier texte.

{{< highlight shell >}}
@echo off

set TM=%date:~6,4%%date:~3,2%%date:~0,2%%time:~0,2%%time:~3,2%%time:~6,2%
rem Zero-pad the hour if it is before 10am
set TM=%TM: =0%
echo %TM%

REM créer un fichier avec OLDNAME/NEWNAME (possibilité de mettre des espaces)
for /F "tokens=1,2 delims=/" %%A in (liste_rename.txt) do (
	rename %%A %%B
	echo %%A %%B > modifs_%TM%.txt
)
PAUSE
{{</ highlight >}}


* `rem` : faire un commentaire dans le .bat
* `set` : définir une variable
* `rename` : renommer
* `for /F` : parcourir un fichier text

Remarque : il me manque une commande pour loguer les erreurs. Par exemple si un fichier est inexistant, dans mon fichier d'export c'est marqué les noms des fichiers.
