---
title: Excel
date: 2020-03-01
tags: ["excel"]
---


# Formule

# Divers
|Formule|Définition|
|:------|:------|
|`SI(plage;"*TEXT*")`|Utiliser `*` signifie que je ne cherche pas un text exact|


* Calculs automatiques ou pas : il faut aller dans le menu « Formule », Option de Calcul  et pas cocher « Manuel ». Pour rafraichir, appuyer sur F9.


## Compter

|Formule|Définition|
|:------|:------|
|`NB.SI($A$1:$A$8;A1)`|Nombre dans une liste|
|`NB.SI($A$1:A1;A1)`|Ordre d'apparition dans une liste (ne pas figer la cellule dans la plage)|
|`SOMMEPROD(1/NB.SI(A1:A8;A1:A8))`|Nombre de valeurs différentes|
|`NB.SI.ENS`|Nombre avec plusieurs colonnes|
|`SOMMEPROD(1*($A$1:$A$8=X1)*($B$1:$B$8=Y1))`|Nombre avec plusieurs colonnes|
|`NB(1/FREQUENCE(SI((fam[ACC_NUMERO]=D2)*(autre critère = "autre");EQUIV(fam[fam_elarg];fam[fam_elarg];0));LIGNE(INDIRECT("1:"&LIGNES(fam[fam_elarg])))))`|Nombre différents avec critères|


## Mise en fomre conditionnelle

|Formule|Définition|
|:------|:------|
|`SI(ET($AO2=0);$AP2="En Cours");1;0)`|Si à 2 cas|
|`=SI(ET(ET(($AK2-30)<AUJOURDHUI();$AO2=0);$AP2="En Cours");1;0)`|Si à 3 cas|
|`(DATE(ANNEE($AE2);MOIS($AE2);1)+30)<AUJOURDHUI()`|Si avec date|
|`DATEDIF`|Si avec date|


## Rechercher une valeur

|Formule|Définition|
|:------|:------|
|`RECHERCHEV`|Afficher une valeur|
|`INDEX("colonne al à afficher";EQUIV("valeur recherchée";"colonne où est la valeur recherchée";0);1 %option)`|Afficher une valeur|
|`INDEX("matrice de recherche totale";EQUIV("valeur recherchée";"plage où est la valeur recherchée";0"recherche la valeur exacte (1 : valeur supérieure / -1 : valeur inférieure)");"numéro de colonne à afficher")`|Rechercher une valeur, avec toute matrice|
|`INDEX(Colonne_resultat;EQUIV(1;(Colonne_critère1="Txt")*(Colonne_critère2=Cellule)*1;0))`|Rechercher une valeur avec plusieurs critères. Validation matricielle : Ctrl + Enter|


Autre exemple - les valeurs suivantes

```
Valeur 1
=INDEX($Index_equi.$C$2:$C$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0))

Valeur 2
=INDEX(DECALER($Index_equi.$C$2:$C$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);EQUIV(H2;DECALER($Index_equi.$A$2:$A$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);0))

Valeur 3
=INDEX(DECALER(DECALER($Index_equi.$C$2:$C$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);EQUIV(H2;DECALER($Index_equi.$A$2:$A$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);0);0);EQUIV(H2;DECALER(DECALER($Index_equi.$A$2:$A$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);EQUIV(H2;DECALER($Index_equi.$A$2:$A$9;EQUIV(H2;$Index_equi.$A$2:$A$9;0);0);0);0);0))
```



# Macro
|Formule|Définition|
|:------|:------|
|`ActiveSheet.DisplayAutomaticPageBreaks = False`|Supprimer les marques d'impression|
|`ActiveSheet.UsedRange`|Réinitialise la dernière valeur d'une feuille|
|`Application.DisplayStatusBar = True` `Application.StatusBar = "toto"`|Status bar|

# Gestion tableau - langage 