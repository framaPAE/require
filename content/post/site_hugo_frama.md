---
title: Créer son site sur framagit
subtitle: en partant du modèle HUGO
date: 2020-01-04
tags: ["html"]
---

# Créer un compte

Se créer un compte sur https://framagit.org/

# Créer le projet

* New Project
* Create from template => Pages/Hugo
* Mettre le nom, en public, et valider.
* Changer la baseurl dans config.toml
    * baseurl => "https://MONUSER.frama.io/monprojet/"
    * title
    * DefaultContentLanguage => fr
    * subtitle

Cliquer sur "Commit changes"


Personnaliser le projet (texte et image dans framagit)
* Aller dans Settings -> General
* Modifier : Project description (optional)
* Modifier : Project avatar
* Possibilité de modifier les accès : Visibility, project features, permissions


# Mon premier post

* Via l'interface web, aller dans Content / post
* Avec le "+" en haut à côté du path, créer un fichier
* "File name" : mon_nom.md (important de mettre l'extension md sinon ça ne sera pas publié)
* Ecrire en markdown

Cliquer sur "Commit changes"


# Personnalisation du site

* Logo : aller sur le fihier et cliquer sur "replace"  themes/beautifulhugo/static/img/avatar-icon.png  (image environ 700*700)
* Logo favoris : themes/beautifulhugo/static/img/favicon.ico (image environ 32*32)
* Aller dans config.toml
	* dateFormat = "2 January 2006"
	* dans [Author] changer ou mettre en commentaire (avec #) voire supprimer la ligne pour ne pas faire apparaitre les logos des GAFAM
	* dans [[menu.main]], remanier les pages à afficher

Cliquer sur "Commit changes"