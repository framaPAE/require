---
title: Outil collaboratif Framasoft
date: 2019-12-01
tags: ["framasoft"]
---

Framasoft propose une suite bureautique en collaboratif.

# Texte 

[framapad.org](https://framapad.org/fr/)

ou
https://mensuel.framapad.org


# Tableur

[framacalc](https://framacalc.org)
