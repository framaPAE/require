---
title: Les commandes FFMPEG
subtitle: pour bidouiller des vidéos
date: 2022-05-23
tags: ["ffmpeg"]
---


# Couper une vidéo

* Couper le début avec `-ss`
> ffmpeg -i input.mp4 -ss 00:00:30 output.mp4

* Couper la fin avec `-t` : mettre le temps de la vidéo
> ffmpeg -i input.mp4 -t 00:01:20 output.mp4

* définir début et temps :
> ffmpeg -i input.mp4 -ss 00:00:30 -t 00:01:20 output.mp4

# Convertir

* scale
> ffmpeg -i input.mp4 -vf scale=720:-1 output.mp4

# Concaténer

1. Créer une liste des vidéos

|files.txt|
|--------|
|file 'input1.mp4'|
|file 'input2.mp4'|

2. Commande
> ffmpeg -i concat -safe 0 -i filess.txt -c copy output.mp4 
