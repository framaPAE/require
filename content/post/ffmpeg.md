---
title: Installer FFMPEG
subtitle: pour bidouiller des vidéos
date: 2020-04-15
tags: ["ffmpeg"]
---

# Installation

## Sur linux MINT

Utiliser la commande ([source](https://linux4one.com/how-to-install-ffmpeg-on-linux-mint-19/)) :
```
sudo apt install ffmpeg
```


# Windows

* Télécharger ffmpeg sur le [site](https://www.ffmpeg.org/download.html#build-windows)
* dézipper
* Prendre le ffmpeg.exe et mettre dans le répertoire où il y a la vidéo
* Ouvrir un terminal
* `cd` le répertoire de travail 


Pour voir les informations :

```bat
ffmpeg -i video.mp4
```
