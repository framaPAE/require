---
title: Utiliser un bot Télégram
date: 2021-02-19
---

# Créer un bot telegram
* Ouvrir telegram sur pc
* Chercher botFather
* Créer bot
* Conserver le token
* Cliquer sur le lien pour ouvrir la conversation avec son bot

# Récupérer id
* Aller sur la page en remplaçant {TOEKN} : https://api.telegram.org/bot{TOKEN}/getUpdates
* Récupérer notamment le CHATID

# Envoyer message
## avec le navigateur
* Taper simplement l'url : https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={CHAT_ID}text=Hello&World

## en shell
* Créer un fichier function. la commande ">/dev/null" permet de ne pas recevoir le retour (on peut enlever pour recevoir le retour telegram)
```shell
#!/bin/bash
function telegram_text_send()
{
	TOKEN=$1
	CHATID=$2
	TEXT=$3
	PARSE_MODE="$4"
	ENDPOINT="sendMessage"
	URL="https://api.telegram.org/bot$TOKEN/$ENDPOINT"

	echo "Envoi message telegram $TEXT"
	curl -s -X POST $URL -d chat_id=$CHATID -d text="$TEXT" -d parse_mode=$PARSE_MODE >/dev/null
}
```
* Créer le fichier qui va appeler la fonction
```shell
#!/bin/bash
source telegram.functions.sh

TOKEN="{TOEKN}"
CHATID="{CHATID}"
TEXT="Hello World"

telegram_text_send $TOKEN $CHATID "$TEXT" "markdown"
}
```